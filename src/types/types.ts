export interface PlaylistDto {
	collaborative: boolean;
	description: string;
	external_urls: { spotify: string };
	href: string;
	id: string;
	images: ImageDto[];
	name: string;
	owner: any;
	primary_color: boolean;
	public: boolean;
	snapshot_id: string;
	tracks: { href: string, total: number };
	type: string;
	uri: string;
}

export interface PlaybackDto {
	actions: { disallows: any };
	context: any;
	currently_playing_type: string;
	device: any;
	is_playing: boolean;
	item: TrackDto;
	progress_ms: number;
	repeat_state: string;
	shuffle_state: boolean;
	timestamp: number;
}

export interface TrackDto {
	album: AlbumDto;
	artists: ArtistDto[];
	available_markets: any[];
	disc_number: number;
	duration_ms: number;
	episode: boolean;
	explicit: boolean;
	external_ids: { isrc: string };
	external_urls: { spotify: string };
	href: string;
	id: string;
	is_local: boolean;
	name: string;
	popularity: number;
	preview_url: string;
	track: boolean;
	track_number: number;
	type: string;
	uri: string;
}

export interface AlbumDto {
	album_type: string;
	artists: ArtistDto[];
	available_markets: any[];
	external_urls: { spotify: string };
	href: string;
	id: string;
	images: ImageDto[];
	name: string;
	release_date: string;
	release_date_precision: string;
	total_tracks: number;
	type: string;
	uri: string;
}

export interface ArtistDto {
	external_urls: { spotify: string };
	href: string;
	id: string;
	name: string;
	type: string;
	uri: string;
}

export interface ImageDto {
	height: number;
	width: number;
	url: string;
}
