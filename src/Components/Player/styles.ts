import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../constants';

export const PlayerContainer = styled.div`
	height: 80%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	margin: 20px;
	border-radius: 5px;
	background: var(--green_1);

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		height: 85%;
		flex-direction: row;
	}
`;
