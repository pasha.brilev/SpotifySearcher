import React from 'react';
import { PlayerButtonContainer, IconConteiner } from '../Buttons/styles';
import { ReactComponent as NextIcon } from 'shared/images/icons/next.svg';
import { ReactComponent as PrevIcon } from 'shared/images/icons/previous.svg';

interface PlayerButtonProps {
	onClick: () => void;
	disabled: boolean;
	isNext?: boolean;
	title?: string;
}

const PlayerButton: React.FC<PlayerButtonProps> = ({
	disabled,
	onClick,
	isNext,
	title,
}) => {
	return (
		<PlayerButtonContainer disabled={disabled} onClick={onClick} title={title}>
			<IconConteiner>
				{isNext ? (
					<NextIcon width={'20px'} height={'20px'} />
				) : (
					<PrevIcon width={'20px'} height={'20px'} />
				)}
			</IconConteiner>
		</PlayerButtonContainer>
	);
};

export default React.memo(PlayerButton);
