import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../../constants';

export const CurrentTrackContainer = styled.div`
	position: relative;
	height: 80%;
	background: var(--green_2);
	padding: 10px;

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		width: 80%;
		height: 100%;
	}
`;

export const DeviceIconContainer = styled.div`
	position: absolute;
	top: 10px;
`;

export const AboutTrack = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	height: 100%;
`;

export const AboutTrackItem = styled.div`
	display: flex;
	justify-content: center;
	width: 100%;

	svg {
		fill: var(--gray_1);
	}
`;

export const AboutTrackText = styled.span`
	margin: 0 0 0 5px;
	color: var(--gray_1);
	font-size: 18px;
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
`;

export const TrackImageContainer = styled.div`
	width: 80%;
	max-width: 160px;

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		max-width: 350px;
	}
`;

export const ImageWrapper = styled.div`
	overflow: hidden;
	position: relative;
	width: 100%;
	padding-bottom: 100%;
	border-radius: 50%;
`;

export const TrackImage = styled.img`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	object-fit: cover;
`;

export const NoTrackText = styled.p`
	position: absolute;
	top: 50%;
	left: 50%;
	margin: 0;
	text-align: center;
	transform: translate(-50%, -50%);
	color: var(--gray_1);
	font-size: 20px;
`;
