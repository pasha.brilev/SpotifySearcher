import React from 'react';
import LazyLoad from 'react-lazyload';
import {
	CurrentTrackContainer,
	DeviceIconContainer,
	AboutTrack,
	AboutTrackItem,
	AboutTrackText,
	TrackImageContainer,
	ImageWrapper,
	TrackImage,
	NoTrackText,
} from './styles';
import { ReactComponent as PcIcon } from 'shared/images/icons/pc.svg';
import { ReactComponent as MobileIcon } from 'shared/images/icons/mobile.svg';
import { ReactComponent as NotIcon } from 'shared/images/icons/not.svg';
import { ReactComponent as AlbumIcon } from 'shared/images/icons/album.svg';
import { ReactComponent as SingerIcon } from 'shared/images/icons/group.svg';
import { ReactComponent as TrackIcon } from 'shared/images/icons/music.svg';
import CurrentTrackButtons from 'Components/Player/CurrentTrack/CurrentTrackButtons/CurrentTrackButtons';
import { getTrackArtists } from 'utils';
import { PlaybackDto } from 'types/types';

interface CurrentTrackProps {
	playback: PlaybackDto;
}

const CurrentTrack: React.FC<CurrentTrackProps> = ({ playback }) => {
	return (
		<CurrentTrackContainer>
			<DeviceIconContainer>
				{!playback && <NotIcon width={'30px'} height={'30px'} />}
				{playback?.device?.type === 'Computer' && (
					<PcIcon width={'30px'} height={'30px'} />
				)}
				{playback?.device?.type === 'Smartphone' && (
					<MobileIcon width={'30px'} height={'30px'} />
				)}
			</DeviceIconContainer>
			<CurrentTrackButtons playback={playback} />
			{playback?.item ? (
				<AboutTrack>
					<TrackImageContainer>
						<ImageWrapper>
							<LazyLoad once>
								<TrackImage src={playback.item.album.images[0].url} alt='' />
							</LazyLoad>
						</ImageWrapper>
					</TrackImageContainer>
					<AboutTrackItem>
						<AlbumIcon width={'25px'} height={'25px'} />
						<AboutTrackText> {playback.item.album.name} </AboutTrackText>
					</AboutTrackItem>
					<AboutTrackItem>
						<SingerIcon width={'25px'} height={'25px'} />
						<AboutTrackText> {getTrackArtists(playback.item)} </AboutTrackText>
					</AboutTrackItem>
					<AboutTrackItem>
						<TrackIcon width={'25px'} height={'25px'} />
						<AboutTrackText> {playback.item.name} </AboutTrackText>
					</AboutTrackItem>
				</AboutTrack>
			) : (
				<NoTrackText>There are no playing tracks</NoTrackText>
			)}
		</CurrentTrackContainer>
	);
};

export default React.memo(CurrentTrack);
