import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getPlayback } from 'redux/actions/playbackActions';
import { setTrack } from 'redux/actions/localeTrackActions';
import { localeTrackSelector } from 'redux/selectors/localeTrackSelector';
import { PlayApi, PauseApi } from 'services';
import { SUCCESS_STATUS } from '../../../../constants';
import { CurrentTrackButtonsContainer } from './styles';
import { TrackButton } from '../../../Buttons/styles';
import { ReactComponent as Update } from 'shared/images/icons/repeat.svg';
import { ReactComponent as Pause } from 'shared/images/icons/pause.svg';
import { ReactComponent as Music } from 'shared/images/icons/music.svg';
import { ReactComponent as Play } from 'shared/images/icons/play.svg';
import { PlaybackDto } from 'types/types';

interface CurrentTrackButtonsProps {
	playback: PlaybackDto;
}

const CurrentTrackButtons: React.FC<CurrentTrackButtonsProps> = ({
	playback,
}) => {
	const dispatch = useDispatch();
	const localeTrack = useSelector(localeTrackSelector);

	const handleUpdateClick = useCallback(() => {
		dispatch(getPlayback());
	}, [dispatch]);

	const handlePlayClick = useCallback(async () => {
		const response = playback?.is_playing
			? await PauseApi.put()
			: await PlayApi.put();
		if (response.status === SUCCESS_STATUS) {
			dispatch(getPlayback());
		}
	}, [dispatch, playback?.is_playing]);

	const handleLocaleTrackClick = useCallback(() => {
		dispatch(setTrack(playback.item));
	}, [dispatch, playback?.item]);

	return (
		<CurrentTrackButtonsContainer>
			<TrackButton onClick={handleUpdateClick}>
				<Update width={'20px'} height={'20px'} />
			</TrackButton>
			{playback ? (
				<TrackButton disabled={!playback} onClick={handlePlayClick}>
					{playback.is_playing ? (
						<Pause width={'20px'} height={'20px'} />
					) : (
						<Play width={'20px'} height={'20px'} />
					)}
				</TrackButton>
			) : (
				<TrackButton
					disabled={!playback}
					title={!playback ? 'Need an active playback' : ''}
				>
					<Play width={'20px'} height={'20px'} />
				</TrackButton>
			)}
			<TrackButton
				disabled={!playback}
				onClick={handleLocaleTrackClick}
				isActive={playback?.item.id === localeTrack?.id}
			>
				<Music width={'20px'} height={'20px'} />
			</TrackButton>
		</CurrentTrackButtonsContainer>
	);
};

export default React.memo(CurrentTrackButtons);
