import styled from 'styled-components';

export const CurrentTrackButtonsContainer = styled.div`
	display: flex;
	flex-direction: column;
	position: absolute;
	right: 5px;
	top: 5px;
`;
