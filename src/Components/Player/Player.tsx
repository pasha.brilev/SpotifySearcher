import React, { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { PlayerContainer } from './styles';
import PlayerButton from 'Components/Player/PlayerButton';
import CurrentTrack from 'Components/Player/CurrentTrack/CurrentTrack';
import { getPlayback } from 'redux/actions/playbackActions';
import { playbackSelector } from 'redux/selectors/playbackSelector';
import { userSelector } from 'redux/selectors/userSelectors';
import { NextTrackApi, PreviousTrackApi } from 'services';
import { SUCCESS_STATUS } from '../../constants';
import { PlaybackDto } from 'types/types';

const Player = () => {
	const dispatch = useDispatch();
	const playback: PlaybackDto = useSelector(playbackSelector);

	const user = useSelector(userSelector);

	const handleNextClick = useCallback(async () => {
		const response = await NextTrackApi.post();
		if (response.status === SUCCESS_STATUS) {
			dispatch(getPlayback());
		}
	}, [dispatch]);

	const handlePreviousClick = useCallback(async () => {
		const response = await PreviousTrackApi.post();
		if (response.status === SUCCESS_STATUS) {
			dispatch(getPlayback());
		}
	}, [dispatch]);

	useEffect(() => {
		dispatch(getPlayback());
	}, [dispatch]);

	return (
		<PlayerContainer>
			<PlayerButton
				disabled={
					!playback ||
					playback?.actions.disallows.skipping_prev ||
					user?.product !== 'premium'
				}
				onClick={handlePreviousClick}
				title={user?.product !== 'premium' ? 'Only for premium users' : ''}
			/>
			<CurrentTrack playback={playback} />
			<PlayerButton
				isNext
				disabled={!playback || user?.product !== 'premium'}
				onClick={handleNextClick}
				title={user?.product !== 'premium' ? 'Only for premium users' : ''}
			/>
		</PlayerContainer>
	);
};

export default React.memo(Player);
