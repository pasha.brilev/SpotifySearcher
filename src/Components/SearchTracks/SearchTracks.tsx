import React, { useState, useEffect, useCallback } from 'react';
import {
	SearchTracksContainer,
	SearchInput,
	SearchResultText,
	SearchResults,
	RefDiv,
} from './styles';
import TrackCard from 'Components/SearchTracks/TrackCard/TrackCard';
import { SearchTracksApi } from 'services';
import useDebounce from 'hooks/useDebounce';
import useElementOnScreen from 'hooks/useElementOnScreen';

const SearchTracks = () => {
	const [search, setSearch] = useState('');
	const debouncedSearchTerm = useDebounce(search, 500);
	const [searchedTracks, setSearchedTracks] = useState<any>({ items: [] });

	const handleChange = useCallback((e: { target: any }) => {
		setSearch(e.target.value);
	}, []);

	const [containerRef, isVisible]: any = useElementOnScreen({
		root: null,
		rootMargin: '10px',
		threshold: 0.1,
	});

	useEffect(() => {
		if (isVisible && searchedTracks?.total > searchedTracks.items.length) {
			SearchTracksApi.get(searchedTracks.items.length, debouncedSearchTerm)
				.then(async (response) => {
					setSearchedTracks((prevState: { items: any }) => {
						let newTracks = response.data?.tracks || response.data;
						return {
							...newTracks,
							items: [...prevState.items, ...newTracks.items],
						};
					});
				})
				.catch(function (error) {
					console.log(error);
				});
		}
	}, [isVisible]);

	useEffect(() => {
		SearchTracksApi.get(0, debouncedSearchTerm)
			.then(async (response) => {
				setSearchedTracks(response.data?.tracks || response.data);
			})
			.catch(function (error) {
				console.log(error);
			});
	}, [debouncedSearchTerm]);

	let hasTracks = searchedTracks?.items?.length;

	return (
		<SearchTracksContainer>
			<SearchInput
				onChange={handleChange}
				value={search}
				placeholder='Type track name...'
			/>
			<SearchResultText>
				{searchedTracks?.total || 0} Tracks were found
			</SearchResultText>
			<SearchResults>
				{hasTracks ? (
					searchedTracks?.items
						?.map((track: any, index: number) => (
							<TrackCard key={track.id + index} track={track} />
						))
						.concat([<RefDiv key='refDiv' ref={containerRef}></RefDiv>])
				) : (
					<SearchResultText>Nothing found</SearchResultText>
				)}
			</SearchResults>
		</SearchTracksContainer>
	);
};

export default React.memo(SearchTracks);
