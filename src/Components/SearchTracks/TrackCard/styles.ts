import styled from 'styled-components';

export const TrackCardContainer = styled.div`
	overflow: hidden;
	position: relative;
	width: 100%;
	height: 0;
	padding-bottom: 100%;
	background: var(--gray_1);
`;

export const TrackHeader = styled.div`
	z-index: 5;
	position: absolute;
	width: 100%;
	background: var(--black_075);
`;

export const TrackFooter = styled.div`
	z-index: 5;
	position: absolute;
	bottom: 0;
	width: 100%;
	padding: 2px;
	background: var(--black_075);
	font-weight: 400;
`;

export const AboutTrackItem = styled.div`
	display: grid;
	grid-template-columns: 10px 1fr;

	p {
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}

	svg {
		fill: var(--gray_1);
	}
`;

export const AboutTrackText = styled.p`
	margin: 0 0 0 5px;
	color: var(--gray_1);
	font-size: 10px;
`;

export const TrackImage = styled.img`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	object-fit: cover;
`;
