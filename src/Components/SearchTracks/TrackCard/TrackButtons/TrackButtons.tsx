import React, { useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { PlayApi, PauseApi } from 'services';
import { playbackSelector } from 'redux/selectors/playbackSelector';
import { localeTrackSelector } from 'redux/selectors/localeTrackSelector';
import { getPlayback } from 'redux/actions/playbackActions';
import { setTrack } from 'redux/actions/localeTrackActions';
import { SUCCESS_STATUS } from '../../../../constants';
import { TrackButtonsContainer } from './styles';
import { TrackButton } from '../../../Buttons/styles';
import { ReactComponent as Play } from 'shared/images/icons/play.svg';
import { ReactComponent as Pause } from 'shared/images/icons/pause.svg';
import { ReactComponent as Music } from 'shared/images/icons/music.svg';
import { TrackDto } from 'types/types';

interface TrackButtonsProps {
	track: TrackDto;
	playlistUri?: string;
	trackPosition?: number;
}

const TrackButtons: React.FC<TrackButtonsProps> = ({
	track,
	playlistUri,
	trackPosition,
}) => {
	const dispatch = useDispatch();
	const playback = useSelector(playbackSelector);
	const localeTrack = useSelector(localeTrackSelector);

	const playContext = useMemo(() => {
		return playlistUri
			? { context_uri: playlistUri, offset: { position: trackPosition } }
			: { uris: [track?.uri] };
	}, [playlistUri, track?.uri, trackPosition]);

	const handlePlayClick = useCallback(async () => {
		const response =
			track?.id === playback?.item?.id
				? await PlayApi.put()
				: await PlayApi.put(playContext);
		if (response.status === SUCCESS_STATUS) {
			dispatch(getPlayback());
		}
	}, [dispatch, playContext, playback?.item?.id, track?.id]);

	const handlePauseClick = useCallback(async () => {
		const response = await PauseApi.put();
		if (response.status === SUCCESS_STATUS) {
			dispatch(getPlayback());
		}
	}, [dispatch]);

	const handleLocaleTrackClick = useCallback(() => {
		dispatch(setTrack(track));
	}, [dispatch, track]);

	return (
		<TrackButtonsContainer>
			<TrackButton
				disabled={!track?.preview_url}
				onClick={handleLocaleTrackClick}
				isActive={track?.id === localeTrack?.id}
			>
				<Music width={'15px'} height={'15px'} />
			</TrackButton>
			{playback?.is_playing && track?.id === playback?.item?.id ? (
				<TrackButton onClick={handlePauseClick}>
					<Pause width={'15px'} height={'15px'} />
				</TrackButton>
			) : (
				<TrackButton
					onClick={handlePlayClick}
					disabled={!playback}
					title={!playback ? 'Need an active playback' : ''}
				>
					<Play width={'15px'} height={'15px'} />
				</TrackButton>
			)}
		</TrackButtonsContainer>
	);
};

export default React.memo(TrackButtons);
