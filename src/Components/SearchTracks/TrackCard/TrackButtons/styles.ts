import styled from 'styled-components';

export const TrackButtonsContainer = styled.div`
	display: flex;
	justify-content: flex-end;
`;
