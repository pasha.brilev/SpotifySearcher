import React from 'react';
import LazyLoad from 'react-lazyload';
import {
	TrackCardContainer,
	TrackHeader,
	TrackFooter,
	TrackImage,
	AboutTrackItem,
	AboutTrackText,
} from './styles';
import { ReactComponent as AlbumIcon } from 'shared/images/icons/album.svg';
import { ReactComponent as SingerIcon } from 'shared/images/icons/group.svg';
import { ReactComponent as TrackIcon } from 'shared/images/icons/music.svg';
import TrackButtons from 'Components/SearchTracks/TrackCard/TrackButtons/TrackButtons';
import { getTrackArtists } from 'utils';
import { TrackDto } from 'types/types';

interface TrackCardProps {
	track: TrackDto;
}

const TrackCard: React.FC<TrackCardProps> = ({ track }) => {
	return (
		<TrackCardContainer>
			<TrackHeader>
				<TrackButtons track={track} />
			</TrackHeader>
			<LazyLoad once overflow>
				<TrackImage src={track?.album.images[0].url} alt='' />
			</LazyLoad>
			<TrackFooter>
				<AboutTrackItem>
					<SingerIcon width={'10px'} height={'10px'} />
					<AboutTrackText> {getTrackArtists(track)} </AboutTrackText>
				</AboutTrackItem>
				<AboutTrackItem>
					<AlbumIcon width={'10px'} height={'10px'} />
					<AboutTrackText> {track?.album.name} </AboutTrackText>
				</AboutTrackItem>
				<AboutTrackItem>
					<TrackIcon width={'10px'} height={'10px'} />
					<AboutTrackText> {track?.name} </AboutTrackText>
				</AboutTrackItem>
			</TrackFooter>
		</TrackCardContainer>
	);
};

export default React.memo(TrackCard);
