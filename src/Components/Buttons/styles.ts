import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../constants';

export const NavButtonWrapper = styled.div`
	width: 30%;

	a {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		height: 90px;
		min-width: 70px;
		font-size: 10px;
		background-color: var(--gray_2);
		color: var(--white);
		text-align: center;

		&:hover > svg {
			fill: var(--green_1);
		}

		${(props: { isActive?: boolean }) =>
			props.isActive &&
			`
        background-color: var(--green_1);

        &:hover > svg {
          fill: var(--white);
        }
      `}

		p {
			display: none;
		}
	}

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		width: 100%;
		height: 30%;

		a {
			height: 100%;

			p {
				display: block;
			}

			&:hover {
				p {
					color: var(--green_1);
				}
			}

			${(props: { isActive?: boolean }) =>
				props.isActive &&
				`
          background-color: var(--green_1);

          &:hover {
            p {
              color: var(--white);
            }
          }
        `}
		}
	}
`;

export const AccountButton = styled.button`
	border: 1px solid var(--green_1);
	border-radius: 5px;

	&: hover > a > svg {
		fill: var(--green_1);
	}
`;

export const PlayerButtonContainer = styled.button`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100%;
	height: 10%;

	&:hover {
		opacity: 0.8;
	}

	&:active {
		transform: scale(0.8);
	}

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		width: 10%;
		height: 100%;
	}

	${(props: { disabled?: boolean }) =>
		props.disabled &&
		`
      background-color: var(--gray_1);
      disabled: true;

      &:hover {
        opacity: 1;
      }

      &:active {
        transform: scale(1);
      }
    `}
`;

export const IconConteiner = styled.div`
	width: 25px;
	height: 25px;
	transform: rotate(90deg);

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		transform: none;
	}
`;

export const SignInConatiner = styled.button`
	border: 1px solid var(--green_1);
	border-radius: 5px;
	padding: 20px;
	background: var(--green_2);
	color: var(--gray_1);

	&:hover {
		opacity: 0.8;
	}

	&: active {
		background: var(--green_1);
	}
`;

export const TrackButton = styled.button`
	padding: 2px 3px;

	&:hover {
		svg {
			fill: var(--green_1);
		}
	}

	${(props: { isActive?: boolean, disabled?: boolean }) =>
		props.isActive &&
		`
          svg {
              fill: var(--green_1);
          }
    `}

	${(props: { isActive?: boolean, disabled?: boolean }) =>
		props.disabled &&
		`
            svg {
                fill: var(--gray_1);
            }

            &:hover {
              svg {
                fill: var(--gray_1);
            }
          }
      `}
`;

export const CloseButtonContainer = styled.button`
    opacity: .5;

    &:hover {
      opacity: 1;
    }
  }
`;
