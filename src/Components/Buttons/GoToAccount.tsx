import React from 'react';
import { AccountButton } from './styles';
import { ReactComponent as Person } from 'shared/images/icons/person.svg';

interface ToAccountButtonProps {
	url: string;
}

const ToAccountButton: React.FC<ToAccountButtonProps> = ({ url }) => {
	return (
		<AccountButton>
			<a href={url} target='_blank' rel='noreferrer'>
				<Person width={'25px'} height={'25px'} />
			</a>
		</AccountButton>
	);
};

export default React.memo(ToAccountButton);
