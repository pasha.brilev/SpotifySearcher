import React from 'react';
import { AccountButton } from './styles';
import { ReactComponent as LogOut } from 'shared/images/icons/log-out.svg';
import { Link } from 'react-router-dom';

interface ExitButtonProps {
	onClick: () => void;
}

const ExitButton: React.FC<ExitButtonProps> = ({ onClick }) => {
	return (
		<AccountButton onClick={onClick}>
			<Link to='/login'>
				<LogOut width={'25px'} height={'25px'} />
			</Link>
		</AccountButton>
	);
};

export default React.memo(ExitButton);
