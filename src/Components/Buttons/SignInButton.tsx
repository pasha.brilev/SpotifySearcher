import React from 'react';
import { SignInConatiner } from './styles';

interface SignInButtonProps {
	onClick: () => void;
}

const SignInButton: React.FC<SignInButtonProps> = ({ onClick }) => {
	return (
		<SignInConatiner onClick={onClick}>Sign In Using Spotify</SignInConatiner>
	);
};

export default React.memo(SignInButton);
