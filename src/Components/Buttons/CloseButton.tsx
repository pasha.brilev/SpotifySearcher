import React from 'react';
import { CloseButtonContainer } from './styles';
import { ReactComponent as Close } from 'shared/images/icons/close.svg';

interface CloseButtonProps {
	onClick: () => void;
}

const CloseButton: React.FC<CloseButtonProps> = ({ onClick }) => {
	return (
		<CloseButtonContainer onClick={onClick}>
			<Close width={'25px'} height={'25px'} />
		</CloseButtonContainer>
	);
};

export default React.memo(CloseButton);
