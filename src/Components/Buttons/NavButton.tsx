import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { NavButtonWrapper } from './styles';

interface NavButtonProps {
	to: string;
	icon: any;
	text: string;
}

const NavButton: React.FC<NavButtonProps> = ({ to, icon, text }) => {
	let match = useRouteMatch({
		path: to,
		exact: true,
	});

	return (
		<NavButtonWrapper isActive={!!match}>
			<Link to={to}>
				{icon}
				<p>{text}</p>
			</Link>
		</NavButtonWrapper>
	);
};

export default React.memo(NavButton);
