import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../constants';

export const HeaderContainer = styled.div`
	width: 100%;

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		display: flex;
		flex-direction: column;
		align-content: space-between;
		width: 15%;
		min-width: 120px;
		height: 100%;
	}
`;
