import React from 'react';
import NavBar from 'Components/Header/NavBar/NavBar';
import Logo from 'Components/Logo/Logo';
import { HeaderContainer } from './styles';

const Header = () => {
	return (
		<HeaderContainer>
			<Logo />
			<NavBar />
		</HeaderContainer>
	);
};

export default React.memo(Header);
