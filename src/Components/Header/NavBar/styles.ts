import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../../constants';

export const NavContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	background-color: var(--gray_3);
	padding: 15px;

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		height: 100%;
	}
`;

export const NavButtonsContainer = styled.div`
	display: flex;
	justify-content: space-between;

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		height: 100%;
		flex-direction: column;
	}
`;

export const NavTitle = styled.p`
	display: none;

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		display: block;
		font-size: 12px;
		margin: 0 auto 5px;
		color: white;
	}
`;
