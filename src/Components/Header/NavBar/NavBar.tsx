import React, { useContext } from 'react';
import NavButton from 'Components/Buttons/NavButton';
import { ReactComponent as Search } from 'shared/images/icons/search.svg';
import { ReactComponent as NowPlaying } from 'shared/images/icons/music.svg';
import { ReactComponent as MyPlaylist } from 'shared/images/icons/menu.svg';
import { ReactComponent as SignIn } from 'shared/images/icons/sign-in.svg';
import { NavContainer, NavTitle, NavButtonsContainer } from './styles';
import { AuthorizedContext } from 'App';

const NavBar = () => {
	const { isAuthorized } = useContext(AuthorizedContext);

	const AUTH_NAVIGATION_LINKS = [
		{
			path: '/search',
			text: 'Search',
			icon: <Search width={'25px'} height={'25px'} />,
		},
		{
			path: '/',
			text: 'Now Playing',
			icon: <NowPlaying width={'25px'} height={'25px'} />,
		},
		{
			path: '/playlists',
			text: 'My Playlist',
			icon: <MyPlaylist width={'25px'} height={'25px'} />,
		},
	];

	const SIGN_IN_LINKS = [
		{
			path: '/login',
			text: 'Sign in',
			icon: <SignIn width={'25px'} height={'25px'} />,
		},
	];

	let visibleButtons = isAuthorized ? AUTH_NAVIGATION_LINKS : SIGN_IN_LINKS;

	return (
		<NavContainer>
			<NavTitle>Navigate</NavTitle>
			<NavButtonsContainer>
				{visibleButtons.map((link) => {
					return (
						<NavButton
							key={link.text}
							to={link.path}
							text={link.text}
							icon={link.icon}
						/>
					);
				})}
			</NavButtonsContainer>
		</NavContainer>
	);
};

export default React.memo(NavBar);
