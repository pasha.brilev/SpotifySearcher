import React, { useContext, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { userSelector } from 'redux/selectors/userSelectors';
import {
	ContentHeaderContainer,
	ContentHeaderText,
	ContentHeaderButtons,
} from './styles';
import ExitButton from 'Components/Buttons/ExitButton';
import GoToAccountButton from 'Components/Buttons/GoToAccount';
import { AuthorizedContext } from 'App';
import { removeLocaleValue } from 'services/storageApi';

interface ContentHeaderProps {
	text: string;
}

const ContentHeader: React.FC<ContentHeaderProps> = ({ text }) => {
	const { isAuthorized, setIsAuthorized } = useContext(AuthorizedContext);
	const user = useSelector(userSelector);

	const ACCOUNT_URL = `https://open.spotify.com/user/${user?.id}`;

	const handleClick = useCallback(() => {
		removeLocaleValue('token');
		setIsAuthorized(false);
	}, [setIsAuthorized]);

	return (
		<ContentHeaderContainer>
			<ContentHeaderText>{text}</ContentHeaderText>
			{isAuthorized && (
				<ContentHeaderButtons>
					<GoToAccountButton url={ACCOUNT_URL} />
					<ExitButton onClick={handleClick} />
				</ContentHeaderButtons>
			)}
		</ContentHeaderContainer>
	);
};

export default React.memo(ContentHeader);
