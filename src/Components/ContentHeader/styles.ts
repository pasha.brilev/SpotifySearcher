import styled from 'styled-components';

export const ContentHeaderContainer = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 10px;
	background: var(--green_2);
`;

export const ContentHeaderButtons = styled.div`
	display: flex;
	justify-content: space-between;
	width: 60px;
`;

export const ContentHeaderText = styled.span`
	color: var(--white);
	font-size: 22px;
`;
