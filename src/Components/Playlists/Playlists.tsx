import React, { useEffect, useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Modal from 'react-bootstrap/Modal';
import {
	PlaylistsContainer,
	SearchInput,
	SearchResultText,
	SearchResults,
} from './styles';
import { RefDiv } from 'Components/SearchTracks/styles';
import Playlist from 'Components/Playlists/Playlist/Playlist';
import PlaylistModal from 'Components/Playlists/PlaylistModal/PlaylistModal';
import { playlistSelector } from 'redux/selectors/playlistSelectors';
import { getPlaylists, clearPlaylists } from 'redux/actions/playlistsActions';
import { PlaylistDto } from 'types/types';
import useDebounce from 'hooks/useDebounce';
import useElementOnScreen from 'hooks/useElementOnScreen';

const Playlists = () => {
	const dispatch = useDispatch();
	const playlists = useSelector(playlistSelector);

	const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
	const [playlist, setPlaylist] = useState<PlaylistDto | null>(null);
	const [search, setSearch] = useState('');
	const [searchedPlaylists, setSearchedPlaylists] = useState<any>([]);

	const debouncedSearchTerm = useDebounce(search, 500);

	const [containerRef, isVisible]: any = useElementOnScreen({
		root: null,
		rootMargin: '10px',
		threshold: 0.1,
	});

	const handleChange = (e: { target: any }) => {
		setSearch(e.target.value);
	};

	const handleClick = useCallback((playlist: PlaylistDto) => {
		setPlaylist(playlist);
		setIsModalOpen(true);
	}, []);

	const handleClose = useCallback(() => {
		setPlaylist(null);
		setIsModalOpen(false);
	}, []);

	useEffect(() => {
		const searchResult = playlists?.items.filter((item: any) =>
			item.name.toUpperCase().includes(debouncedSearchTerm.toUpperCase())
		);
		setSearchedPlaylists(searchResult);
	}, [playlists, debouncedSearchTerm]);

	useEffect(() => {
		if (isVisible && playlists?.total > playlists.items.length) {
			dispatch(getPlaylists(playlists.items.length));
		}
	}, [isVisible]);

	useEffect(() => {
		dispatch(getPlaylists(0));
		return () => {
			dispatch(clearPlaylists());
		};
	}, [dispatch]);

	return (
		<>
			<PlaylistsContainer>
				<SearchInput
					onChange={handleChange}
					value={search}
					placeholder='Search in loaded playlists'
				/>
				<SearchResultText>
					<span>{searchedPlaylists?.length}</span>/{playlists?.total} playlists
					visible
				</SearchResultText>
				<SearchResults>
					{searchedPlaylists
						?.map((playlist: PlaylistDto) => (
							<Playlist
								key={playlist.id}
								onClick={() => handleClick(playlist)}
								name={playlist.name}
							/>
						))
						.concat([<RefDiv key='refDiv' ref={containerRef}></RefDiv>])}
				</SearchResults>
			</PlaylistsContainer>

			<Modal
				show={isModalOpen}
				size='lg'
				centered
				scrollable
				fullscreen='md-down'
			>
				<PlaylistModal playlist={playlist} onClose={handleClose} />
			</Modal>
		</>
	);
};

export default React.memo(Playlists);
