import React from 'react';
import { PlaylistContainer } from './styles';

interface PlaylistProps {
	name: string;
	onClick(): void;
}

const Playlist: React.FC<PlaylistProps> = ({ name, onClick }) => {
	return <PlaylistContainer onClick={onClick}>{name}</PlaylistContainer>;
};

export default React.memo(Playlist);
