import styled from 'styled-components';

export const PlaylistContainer = styled.div`
	padding: 5px;
	margin-bottom: 10px;
	border-radius: 5px;
	border-left: 10px solid var(--green_1);
	background: linear-gradient(to right, var(--gray_2), var(--gray_3));
	color: var(--gray_1);
	text-transform: uppercase;
	cursor: pointer;

	&:hover {
		color: var(--white);
	}
`;
