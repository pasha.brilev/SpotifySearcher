import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../constants';

export const PlaylistsContainer = styled.div`
	height: 90%;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding-top: 15px;
`;

export const SearchInput = styled.input`
	width: 80%;
	height: 35px;
	background-color: var(--gray_3);
	border: 1px solid var(--green_1);
	border-radius: 5px;
	padding-left: 10px;
	color: var(--white);
`;

export const SearchResultText = styled.p`
	font-size: 14px;
	color: var(--gray_1);
	margin: 8px 0;

	span {
		color: var(--green_1);
	}

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		margin: 16px 0;
	}
`;

export const SearchResults = styled.div`
	height: 80%;
	width: 100%;
	padding: 0 15px;
	overflow: auto;
`;
