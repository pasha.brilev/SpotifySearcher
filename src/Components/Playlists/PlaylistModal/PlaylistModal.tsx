import React, { useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Switch from 'react-switch';
import {
	PlaylistModalHeader,
	PlaylistModalTitle,
	PlaylistInfoContainer,
	PlaylistImageContainer,
	PlaylistInfo,
	PlaylistTitle,
	SwitchContainer,
	PlaylistData,
	ImageWrapper,
	PlaylistImage,
} from './styles';
import { RefDiv } from 'Components/SearchTracks/styles';
import CloseButton from 'Components/Buttons/CloseButton';
import TrackFromPlaylist from 'Components/Playlists/PlaylistModal/TrackFromPlaylist/TrackFromPlaylist';
import { PlaylistDto, TrackDto } from 'types/types';
import {
	getTraksFromPlaylist,
	clearTraksFromPlaylist,
} from 'redux/actions/traksFromPlaylistActions';
import { traksFromPlaylistSelector } from 'redux/selectors/traksFromPlaylistSelectors';
import useElementOnScreen from 'hooks/useElementOnScreen';

interface PlaylistModalProps {
	playlist: PlaylistDto | null;
	onClose(): void;
}

const PlaylistModal: React.FC<PlaylistModalProps> = ({ playlist, onClose }) => {
	const dispatch = useDispatch();

	const tracksItems = useSelector(traksFromPlaylistSelector);

	const [smartView, setSmartView] = useState(false);

	const [containerRef, isVisible]: any = useElementOnScreen({
		root: null,
		rootMargin: '10px',
		threshold: 0.1,
	});

	const handleSwitch = useCallback(() => {
		setSmartView((prewState) => !prewState);
	}, []);

	const handleClose = useCallback(() => {
		dispatch(clearTraksFromPlaylist());
		onClose();
	}, [dispatch, onClose]);

	useEffect(() => {
		playlist && dispatch(getTraksFromPlaylist(playlist.id, 0));
	}, [dispatch, playlist]);

	useEffect(() => {
		if (isVisible && tracksItems?.total > tracksItems.items.length) {
			playlist &&
				dispatch(getTraksFromPlaylist(playlist.id, tracksItems.items.length));
		}
	}, [isVisible]);

	const tracks = tracksItems?.items.map(
		(item: { track: TrackDto }) => item.track
	);

	return (
		<>
			<PlaylistModalHeader>
				<PlaylistModalTitle id='contained-modal-title-vcenter'>
					{playlist?.name}
				</PlaylistModalTitle>
				<CloseButton onClick={handleClose} />
			</PlaylistModalHeader>
			<PlaylistInfoContainer>
				<PlaylistImageContainer>
					<ImageWrapper>
						<PlaylistImage src={playlist?.images[0]?.url} alt='' />
					</ImageWrapper>
				</PlaylistImageContainer>
				<PlaylistInfo>
					<SwitchContainer>
						<Switch
							onChange={handleSwitch}
							checked={smartView}
							height={20}
							width={40}
							handleDiameter={20}
							uncheckedIcon={false}
							checkedIcon={false}
						/>
						{smartView ? <p>Smart View</p> : <p>Full View</p>}
					</SwitchContainer>

					<PlaylistTitle>
						<p>{playlist?.name}</p>
						<p>
							<span>{tracksItems?.items?.length}</span>/{tracksItems?.total}{' '}
							Tracks
						</p>
					</PlaylistTitle>
				</PlaylistInfo>
			</PlaylistInfoContainer>
			<PlaylistData smartView={smartView}>
				{tracks
					?.map((track: any, index: number) => (
						<TrackFromPlaylist
							key={track.id + index}
							track={track}
							playlistUri={playlist?.uri}
							trackPosition={index}
							smartView={smartView}
						/>
					))
					.concat([<RefDiv key='refDiv' ref={containerRef}></RefDiv>])}
			</PlaylistData>
		</>
	);
};

export default React.memo(PlaylistModal);
