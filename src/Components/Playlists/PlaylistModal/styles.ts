import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../../constants';

export const PlaylistModalHeader = styled.div`
	display: flex;
	justify-content: space-between;
	padding: 10px;
	background-color: var(--green_1);
	color: var(--white);
`;

export const PlaylistModalTitle = styled.div`
	text-transform: uppercase;
`;

export const PlaylistModalBody = styled.div`
	background-color: var(--gray_2);
	padding: 10px;
`;

export const PlaylistInfoContainer = styled.div`
	display: flex;
	background-color: var(--gray_2);
	padding: 10px;
`;

export const PlaylistImageContainer = styled.div`
	width: 70%;
	max-width: 200px;
	display: flex;
	align-items: center;
`;

export const ImageWrapper = styled.div`
	overflow: hidden;
	position: relative;
	width: 100%;
	padding-bottom: 100%;
`;

export const PlaylistImage = styled.img`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
`;

export const PlaylistInfo = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	width: 100%;
`;

export const PlaylistTitle = styled.div`
	text-align: center;
	font-size: 18px;
	color: var(--gray_1);

	p {
		margin: 0;
	}

	span {
		color: var(--green_1);
	}

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		font-size: 22px;
	}
`;

export const SwitchContainer = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: 15px;

	p {
		margin: 0 0 0 5px;
		color: var(--gray_1);
	}
`;

export const PlaylistData = styled.div`
	background-color: var(--gray_2);
	padding: 0 10px 0;
	height: 100%;
	overflow-y: auto;

	${(props: { smartView: boolean }) =>
		props.smartView &&
		`
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
        `}
`;
