import styled from 'styled-components';

export const TrackFromPlaylistContainer = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	height: 40px;
	margin: 0 0 10px 10px;
	background-color: var(--green_2);
	border-radius: 5px;
	padding-right: 5px;

	&:last-child {
		margin-right: auto;
	}
`;

export const TrackSmallImage = styled.img`
	width: 40px;
	height: 40px;
	object-fit: cover;
	border-radius: 5px;
`;

export const TrackName = styled.span`
	color: var(--white);
	width: 100%;
	margin-left: 5px;
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;

	${(props: { smartView?: boolean }) =>
		props.smartView &&
		`
        display: none;
      `}
`;
