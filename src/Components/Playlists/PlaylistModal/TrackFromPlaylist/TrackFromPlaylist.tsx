import React from 'react';
import LazyLoad from 'react-lazyload';
import {
	TrackFromPlaylistContainer,
	TrackSmallImage,
	TrackName,
} from './styles';
import TrackButtons from 'Components/SearchTracks/TrackCard/TrackButtons/TrackButtons';
import { getTrackArtists } from 'utils';
import { TrackDto } from 'types/types';

interface TrackFromPlaylistProps {
	track: TrackDto;
	smartView: boolean;
	playlistUri?: string;
	trackPosition: number;
}

const TrackFromPlaylist: React.FC<TrackFromPlaylistProps> = ({
	track,
	smartView,
	playlistUri,
	trackPosition,
}) => {
	const trackArtist = getTrackArtists(track);

	return (
		<TrackFromPlaylistContainer>
			<LazyLoad once overflow>
				<TrackSmallImage src={track.album.images[2].url} alt='' />
			</LazyLoad>
			<TrackName smartView={smartView}>
				{trackArtist} - {track.name}{' '}
			</TrackName>
			<TrackButtons
				track={track}
				playlistUri={playlistUri}
				trackPosition={trackPosition}
			/>
		</TrackFromPlaylistContainer>
	);
};

export default React.memo(TrackFromPlaylist);
