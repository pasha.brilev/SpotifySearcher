import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../constants';

export const LocalePlayerContainer = styled.div`
	display: flex;
	flex-direction: column;
	position: absolute;
	bottom: 0;
	right: 0;
	z-index: 10;
	background: var(--gray_3);
	width: 100%;
	max-width: 350px;

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		width: 30%;
	}
`;

export const LocalePlayerButtons = styled.div`
	display: flex;
	justify-content: space-around;
	align-items: center;
	height: 35px;
	width: 100%;
`;

export const TimeLineGray = styled.div`
	position: relative;
	height: 3px;
	width: 100%;
	background-color: var(--gray_1);
`;

export const TimeLine = styled.div`
	${(props: { width: number }) =>
		`
        position: absolute;
        top: 0;
        left: 0;
        height: 3px;
        width: ${props.width * 100}%;
        background-color: var(--white);
    `}
`;

export const LocalePlayerButton = styled.button`
	&:hover {
		svg {
			fill: var(--gray_1);
		}
	}

	${(props: { isActive?: boolean }) =>
		props.isActive &&
		`
          svg {
              fill: var(--green_1);
          }
    `}
`;

export const TrackData = styled.div`
	width: 30%;
	color: white;
	font-size: 10px;
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
`;

export const CloseButton = styled.button`
	position: absolute;
	top: 0;
	right: 0;
	z-index: 20;
	margin-right: 5px;
	svg {
		fill: var(--gray_2);
	}

	&:hover {
		svg {
			fill: var(--black);
		}
	}
`;

export const TrackImageContainer = styled.div`
	display: none;

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		display: block;

		width: 100%;
	}
`;

export const ImageWrapper = styled.div`
	overflow: hidden;
	position: relative;
	width: 100%;
	padding-bottom: 90%;
`;

export const TrackImage = styled.img`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	object-fit: cover;
`;
