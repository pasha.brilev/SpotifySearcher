import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import LazyLoad from 'react-lazyload';
import {
	LocalePlayerContainer,
	LocalePlayerButtons,
	TimeLine,
	TimeLineGray,
	LocalePlayerButton,
	TrackData,
	TrackImageContainer,
	ImageWrapper,
	TrackImage,
	CloseButton,
} from './styles';
import { clearLocaleTrack } from 'redux/actions/localeTrackActions';
import { localeTrackSelector } from 'redux/selectors/localeTrackSelector';
import { ReactComponent as Close } from 'shared/images/icons/close.svg';
import { ReactComponent as Repeat } from 'shared/images/icons/repeat-2.svg';
import { ReactComponent as Pause } from 'shared/images/icons/pause.svg';
import { ReactComponent as Play } from 'shared/images/icons/play.svg';
import { ReactComponent as NextIcon } from 'shared/images/icons/next.svg';
import { ReactComponent as PrevIcon } from 'shared/images/icons/previous.svg';
import { ReactComponent as Unmute } from 'shared/images/icons/volume.svg';
import { ReactComponent as Mute } from 'shared/images/icons/mute.svg';

const LocalePlayer = () => {
	const dispatch = useDispatch();
	const localeTrack = useSelector(localeTrackSelector);
	const audio = useRef(null);

	const [isPlaying, setIsPlaying] = useState(true);
	const [isRepeat, setIsRepeat] = useState(false);
	const [isMute, setIsMute] = useState(false);
	const [currentTime, setCurrentTime] = useState(0);

	useEffect(() => {
		let interval = setInterval(() => {
			//@ts-ignore
			setCurrentTime(Math.round(audio.current?.currentTime));
		}, 1000);
		return () => clearInterval(interval);
	}, []);

	useEffect(() => {
		setIsPlaying(true);
	}, [localeTrack]);

	if (!localeTrack) {
		return null;
	}

	const handleClose = () => {
		dispatch(clearLocaleTrack());
	};

	const handlePlay = () => {
		setIsPlaying((prevState) => !prevState);
		//@ts-ignore
		audio && isPlaying ? audio.current.pause() : audio.current.play();
	};

	const handleRepeat = () => {
		setIsRepeat((prevState) => {
			//@ts-ignore
			audio.current.loop = !prevState;
			return !prevState;
		});
	};

	const handleMute = () => {
		setIsMute((prevState) => {
			//@ts-ignore
			audio.current.muted = !prevState;
			return !prevState;
		});
	};

	//@ts-ignore
	let duration = Math.round(audio.current?.duration);

	//@ts-ignore
	let lineWidth = currentTime / duration;

	return (
		<LocalePlayerContainer>
			<audio ref={audio} autoPlay src={localeTrack?.preview_url}>
				Your browser does not support the
				<code>audio</code> element.
			</audio>

			<CloseButton>
				<Close width={'15px'} onClick={handleClose} />
			</CloseButton>
			<TrackImageContainer>
				<ImageWrapper>
					<LazyLoad once>
						<TrackImage src={localeTrack?.album.images[0].url} alt='' />
					</LazyLoad>
				</ImageWrapper>
			</TrackImageContainer>
			<TimeLineGray>
				<TimeLine width={lineWidth || 0} />
			</TimeLineGray>
			<LocalePlayerButtons>
				<LocalePlayerButton>
					<PrevIcon width={'10px'} />
				</LocalePlayerButton>
				<LocalePlayerButton onClick={handlePlay}>
					{isPlaying ? <Pause width={'15px'} /> : <Play width={'15px'} />}
				</LocalePlayerButton>
				<LocalePlayerButton>
					<NextIcon width={'10px'} />
				</LocalePlayerButton>
				<LocalePlayerButton isActive={isRepeat} onClick={handleRepeat}>
					<Repeat width={'15px'} />
				</LocalePlayerButton>
				<LocalePlayerButton onClick={handleMute}>
					{isMute ? <Mute width={'15px'} /> : <Unmute width={'15px'} />}
				</LocalePlayerButton>
				<TrackData>
					<div>{localeTrack?.name}</div>
					<div>
						0:{currentTime} / 0:{duration}
					</div>
				</TrackData>
			</LocalePlayerButtons>
		</LocalePlayerContainer>
	);
};

export default LocalePlayer;
