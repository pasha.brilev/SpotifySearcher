import React from 'react';
import { Link } from 'react-router-dom';
import { ReactComponent as LogoIcon } from 'shared/images/icons/spotify.svg';
import { LogoContainer } from './styles';

const Logo = () => {
	return (
		<LogoContainer>
			<Link to='/search'>
				<LogoIcon width={'24px'} height={'24px'} />
				<p>Spotify Search</p>
			</Link>
		</LogoContainer>
	);
};

export default React.memo(Logo);
