import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../constants';

export const LogoContainer = styled.div`
	width: 100%;
	padding-bottom: 10px;

	a {
		display: flex;
		justify-content: center;
		align-items: center;
		width: 100%;
		height: 40px;
		font-size: 20px;
		background-color: var(--gray_3);
		color: var(--green_1);

		&:hover {
			opacity: 0.9;
		}

		p {
			margin: 0 0 0 10px;
		}

		@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
			flex-direction: column;
			height: 100px;
			width: 100%;
			min-width: 90px;
			font-size: 16px;
			text-align: center;

			p {
				margin: 10px 0 0 0;
				width: 90px;
			}
		}
	}
`;
