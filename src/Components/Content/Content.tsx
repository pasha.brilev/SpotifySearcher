import React, { useContext, useEffect, Suspense, lazy } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { ContentContainer } from './styles';
import { AuthorizedContext } from 'App';
import { getUser } from 'redux/actions/userActions';
import { getPlayback } from 'redux/actions/playbackActions';

const NowPlayingPage = lazy(
	() => import('pages/NowPlayingPage/NowPlayingPage')
);
const SearchPage = lazy(() => import('pages/SearchPage/SearchPage'));
const MyPlaylistsPage = lazy(
	() => import('pages/MyPlaylistsPage/MyPlaylistsPage')
);
const LoginPage = lazy(() => import('pages/LoginPage/LoginPage'));

const Content = () => {
	const dispatch = useDispatch();

	const { isAuthorized } = useContext(AuthorizedContext);

	useEffect(() => {
		if (isAuthorized) {
			dispatch(getUser());
			dispatch(getPlayback());
		}
	}, [dispatch, isAuthorized]);

	return (
		<Suspense fallback={<div>Loading...</div>}>
			<Switch>
				<ContentContainer>
					{!isAuthorized ? (
						<LoginPage />
					) : (
						<>
							<Route path='/' exact>
								{!isAuthorized ? <Redirect to='/login' /> : <NowPlayingPage />}
							</Route>
							<Route path='/search'>
								{!isAuthorized ? <Redirect to='/login' /> : <SearchPage />}
							</Route>
							<Route path='/playlists'>
								{!isAuthorized ? <Redirect to='/login' /> : <MyPlaylistsPage />}
							</Route>
							<Route path='/login'>
								<LoginPage />
							</Route>
						</>
					)}
				</ContentContainer>
			</Switch>
		</Suspense>
	);
};

export default React.memo(Content);
