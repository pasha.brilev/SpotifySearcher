import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../constants';

export const ContentContainer = styled.div`
	width: 100%;
	height: 70%;
	margin: 20px 0 0 0;
	padding: 15px 10px;
	background: var(--gray_2);

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		height: 100%;
		margin: 0 0 0 20px;
	}
`;
