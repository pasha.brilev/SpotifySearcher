import { useState, useEffect } from 'react';

export default function useDebounce<TypeOfValue>(
	value: TypeOfValue,
	delay: number
): TypeOfValue {
	const [debouncedValue, setDebouncedValue] = useState(value);
	useEffect(() => {
		const handler = setTimeout(() => {
			setDebouncedValue(value);
		}, delay);
		return () => {
			clearTimeout(handler);
		};
	}, [value, delay]);
	return debouncedValue;
}
