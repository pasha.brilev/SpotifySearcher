import { useState, useEffect } from 'react';
import { LogInApi, queryParams } from 'services';
import { setLocaleValue, getLocaleValue } from 'services/storageApi';

export default function useAuthorization() {
	const isAuth = !!getLocaleValue('token');

	const [isAuthorized, setIsAuthorized] = useState(isAuth);

	useEffect(() => {
		const genAuth = async () => {
			if (!isAuth && queryParams.code) {
				try {
					const { data: response } = await LogInApi.get();

					setLocaleValue('token', response.access_token);
					setIsAuthorized(true);
				} catch (error) {
					console.log(error);
					throw error;
				}
			} else {
				setIsAuthorized(isAuth);
			}
		};

		genAuth();
	}, [isAuth]);

	return [isAuthorized, setIsAuthorized];
}
