import styled, { createGlobalStyle } from 'styled-components';
import { MOBILE_BREAKPOINT } from './constants';

export default createGlobalStyle`
  :root {
    --white: #ffffff;
    --gray_1: #808080;
    --gray_2: #37393a;
    --gray_3: #2b2d2e;
    --black: #000000;
    --black_075: #000000bf;
    --green_1: #9ba060;
    --green_2: #41433f;
  }

  * {
    box-sizing: border-box;

    &::-webkit-scrollbar {
        width: 0;
    }
    @media screen and (min-width: ${MOBILE_BREAKPOINT}) {

      &::-webkit-scrollbar {
          width: 5px;
          background-color: var(--gray_2);
      }

      &::-webkit-scrollbar-thumb {
          border-radius: 5px;
          background-color: var(--green_1);
      }
  
      &::-webkit-scrollbar-track {
          border-radius: 5px;
          background-color: var(--gray_2);
      }
    }
  }

  body {
      margin: 0;
      padding: 0;
      color: var(--black);
      font-weight: 100;
  }

  a {
    text-decoration: none;
    
      &:visited,  
      &:hover,
      &:active {
        text-decoration: none;
      }
  }

  button {
    padding: 0;
    border: none;
    font: inherit;
    color: inherit;
    background-color: transparent;
    cursor: pointer;
  }
`;

export const MainContainer = styled.div`
	display: flex;
	flex-direction: column;
	height: 100vh;
	padding: 25px;
	background: linear-gradient(to right, var(--gray_2), black);

	@media screen and (min-width: ${MOBILE_BREAKPOINT}) {
		flex-direction: row;
	}
`;
