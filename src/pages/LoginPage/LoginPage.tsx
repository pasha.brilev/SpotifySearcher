import React, { useCallback } from 'react';
import { LoginContainer, SignInButtonContainer } from './styles';
import ContentHeader from 'Components/ContentHeader/ContentHeader';
import SignInButton from 'Components/Buttons/SignInButton';
import { AUTHORIZE_URL } from 'services/constants';

const LogIn = () => {
	const handleLogin = useCallback(() => {
		window.location.href = AUTHORIZE_URL.href;
	}, []);

	return (
		<LoginContainer>
			<ContentHeader text='Login' />
			<SignInButtonContainer>
				<SignInButton onClick={handleLogin} />
			</SignInButtonContainer>
		</LoginContainer>
	);
};

export default React.memo(LogIn);
