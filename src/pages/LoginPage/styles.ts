import styled from 'styled-components';

export const LoginContainer = styled.div`
	position: relative;
	height: 100%;
`;

export const SignInButtonContainer = styled.div`
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
`;
