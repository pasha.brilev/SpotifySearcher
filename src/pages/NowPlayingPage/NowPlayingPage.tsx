import React from 'react';
import { NowPlayingContainer } from './styles';
import ContentHeader from 'Components/ContentHeader/ContentHeader';
import Player from 'Components/Player/Player';

const NowPlaying = () => {
	return (
		<NowPlayingContainer>
			<ContentHeader text='Now Playing' />
			<Player />
		</NowPlayingContainer>
	);
};

export default React.memo(NowPlaying);
