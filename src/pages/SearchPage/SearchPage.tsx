import React from 'react';
import { SearchContainer } from './styles';
import ContentHeader from 'Components/ContentHeader/ContentHeader';
import SearchTracks from 'Components/SearchTracks/SearchTracks';

const Search = () => {
	return (
		<SearchContainer>
			<ContentHeader text='Search' />
			<SearchTracks />
		</SearchContainer>
	);
};

export default React.memo(Search);
