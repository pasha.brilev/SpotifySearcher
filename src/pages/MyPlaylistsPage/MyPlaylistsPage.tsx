import React from 'react';
import { MyPlaylistsContainer } from './styles';
import ContentHeader from 'Components/ContentHeader/ContentHeader';
import Playlists from 'Components/Playlists/Playlists';

const Search = () => {
	return (
		<MyPlaylistsContainer>
			<ContentHeader text='My Playlists' />
			<Playlists />
		</MyPlaylistsContainer>
	);
};

export default React.memo(Search);
