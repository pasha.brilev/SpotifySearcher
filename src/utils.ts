import { TrackDto } from 'types/types';

export const getTrackArtists = (track: TrackDto) => {
	return track?.artists?.map((item) => item.name).join(' & ');
};
