import { Action, createActions } from 'redux-actions';
import { PlaylistsApi } from 'services';

const { getPlaylistsSuccessful, getPlaylistsError, clearPlaylistsState } =
	createActions({
		GET_PLAYLISTS_SUCCESSFUL: (data) => ({ data }),
		GET_PLAYLISTS_ERROR: (error) => ({ error }),
		CLEAR_PLAYLISTS_STATE: () => ({}),
	});

const getPlaylists =
	(offset: number) => async (dispatch: (arg0: Action<{}>) => void) => {
		try {
			const { data: response } = await PlaylistsApi.get(offset);
			dispatch(getPlaylistsSuccessful(response));
		} catch (error) {
			dispatch(getPlaylistsError(error));
			throw error;
		}
	};

const clearPlaylists = () => (dispatch: (arg0: Action<{}>) => void) => {
	dispatch(clearPlaylistsState());
};

export {
	getPlaylists,
	clearPlaylists,
	getPlaylistsSuccessful,
	getPlaylistsError,
	clearPlaylistsState,
};
