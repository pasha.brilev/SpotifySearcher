import { Action, createActions } from 'redux-actions';
import { PlaybackApi } from 'services';

const { getPlaybackSuccessful, getPlaybackError } = createActions({
	GET_PLAYBACK_SUCCESSFUL: (data) => ({ data }),
	GET_PLAYBACK_ERROR: (error) => ({ error }),
});

const getPlayback = () => async (dispatch: (arg0: Action<{}>) => void) => {
	try {
		const { data: response } = await PlaybackApi.get();
		dispatch(getPlaybackSuccessful(response));
	} catch (error) {
		dispatch(getPlaybackError(error));
		throw error;
	}
};

export { getPlayback, getPlaybackSuccessful, getPlaybackError };
