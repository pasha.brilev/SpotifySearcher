import { Action, createActions } from 'redux-actions';
import { CurrentUserApi } from 'services';

const { getUserSuccessful, getUserError } = createActions({
	GET_USER_SUCCESSFUL: (data) => ({ data }),
	GET_USER_ERROR: (error) => ({ error }),
});

const getUser = () => async (dispatch: (arg0: Action<{}>) => void) => {
	try {
		const { data: response } = await CurrentUserApi.get();
		dispatch(getUserSuccessful(response));
	} catch (error) {
		dispatch(getUserError(error));
		throw error;
	}
};

export { getUser, getUserSuccessful, getUserError };
