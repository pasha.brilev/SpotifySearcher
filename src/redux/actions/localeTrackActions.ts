import { Action, createActions } from 'redux-actions';
import { TrackDto } from 'types/types';

const { setLocaleTrack, clearLocaleTrackState } = createActions({
	SET_LOCALE_TRACK: (data: TrackDto) => ({ data }),
	CLEAR_LOCALE_TRACK_STATE: () => ({}),
});

const setTrack =
	(track: TrackDto) => (dispatch: (arg0: Action<{}>) => void) => {
		if (!track) {
			return null;
		}
		dispatch(setLocaleTrack(track));
	};

const clearLocaleTrack = () => (dispatch: (arg0: Action<{}>) => void) => {
	dispatch(clearLocaleTrackState());
};

export { setTrack, clearLocaleTrack, setLocaleTrack, clearLocaleTrackState };
