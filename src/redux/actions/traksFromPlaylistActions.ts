import { Action, createActions } from 'redux-actions';
import { TraksFromPlaylistApi } from 'services';

const {
	getTracksFromPlaylistSuccessful,
	getTracksFromPlaylistError,
	clearTracks,
} = createActions({
	GET_TRACKS_FROM_PLAYLIST_SUCCESSFUL: (data) => ({ data }),
	GET_TRACKS_FROM_PLAYLIST_ERROR: (error) => ({ error }),
	CLEAR_TRACKS: () => ({}),
});

const getTraksFromPlaylist =
	(playlistId: string, offset: number) =>
	async (dispatch: (arg0: Action<{}>) => void) => {
		try {
			const { data: response } = await TraksFromPlaylistApi.get(
				playlistId,
				offset
			);
			dispatch(getTracksFromPlaylistSuccessful(response));
		} catch (error) {
			dispatch(getTracksFromPlaylistError(error));
			throw error;
		}
	};

const clearTraksFromPlaylist = () => (dispatch: (arg0: Action<{}>) => void) => {
	dispatch(clearTracks());
};

export {
	getTraksFromPlaylist,
	clearTraksFromPlaylist,
	getTracksFromPlaylistSuccessful,
	getTracksFromPlaylistError,
	clearTracks,
};
