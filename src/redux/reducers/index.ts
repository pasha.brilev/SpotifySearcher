import user from './userReducer';
import playlists from './playlistsReducer';
import tracksFromPlaylist from './tracksFromPlaylistReducer';
import playback from './playbackReducer';
import localeTrack from './localeTrackReducer';
import { CombinedState, combineReducers } from 'redux';
import { Action } from 'redux-actions';

const appReducer = combineReducers({
	user,
	playlists,
	tracksFromPlaylist,
	playback,
	localeTrack,
});

const rootReducer = (
	state:
		| CombinedState<{
				user: CombinedState<{ user: { data: any, error: any } }>,
				playlists: CombinedState<{ playlists: { data: any, error: any } }>,
				tracksFromPlaylist: CombinedState<{
					playlistTracks: { data: any, error: any },
				}>,
				playback: CombinedState<{ playback: { data: any, error: any } }>,
				localeTrack: CombinedState<{ localeTrack: { data: any, error: any } }>,
		  }>
		| undefined,
	action: Action<{ data: any, error: any }>
) => {
	return appReducer(state, action);
};

export default rootReducer;
