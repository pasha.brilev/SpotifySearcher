import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import {
	getPlaylistsSuccessful,
	getPlaylistsError,
	clearPlaylistsState,
} from '../actions/playlistsActions';
import { setClearState, setRejectedState } from '../utils/cases';

const playlists = handleActions(
	{
		[String(getPlaylistsSuccessful)]: (state, { payload }) => {
			return {
				error: null,
				//@ts-ignore
				data: state.data
					? {
							...state.data,
							items: [...state.data?.items, ...payload.data?.items],
					  }
					: payload.data,
			};
		},
		[String(getPlaylistsError)]: setRejectedState,
		[String(clearPlaylistsState)]: setClearState,
	},
	{
		data: null,
		error: null,
	}
);

export default combineReducers({
	playlists,
});
