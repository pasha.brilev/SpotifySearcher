import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import {
	getTracksFromPlaylistSuccessful,
	getTracksFromPlaylistError,
	clearTracks,
} from '../actions/traksFromPlaylistActions';
import { setClearState, setRejectedState } from '../utils/cases';

const playlistTracks = handleActions(
	{
		[String(getTracksFromPlaylistSuccessful)]: (state, { payload }) => {
			return {
				...state,
				//@ts-ignore
				data: state.data
					? {
							...state.data,
							items: [...state.data?.items, ...payload.data?.items],
					  }
					: payload.data,
			};
		},
		[String(getTracksFromPlaylistError)]: setRejectedState,
		[String(clearTracks)]: setClearState,
	},
	{
		data: null,
		error: null,
	}
);

export default combineReducers({
	playlistTracks,
});
