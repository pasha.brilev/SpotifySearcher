import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import {
	getPlaybackSuccessful,
	getPlaybackError,
} from '../actions/playbackActions';
import { setFulfilledState, setRejectedState } from '../utils/cases';

const playback = handleActions(
	{
		[String(getPlaybackSuccessful)]: setFulfilledState,
		[String(getPlaybackError)]: setRejectedState,
	},
	{
		data: null,
		error: null,
	}
);

export default combineReducers({
	playback,
});
