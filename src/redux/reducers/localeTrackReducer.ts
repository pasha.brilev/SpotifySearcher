import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import {
	setLocaleTrack,
	clearLocaleTrackState,
} from '../actions/localeTrackActions';
import { setClearState, setFulfilledState } from '../utils/cases';

const localeTrack = handleActions(
	{
		[String(setLocaleTrack)]: setFulfilledState,
		[String(clearLocaleTrackState)]: setClearState,
	},
	{
		data: null,
		error: null,
	}
);

export default combineReducers({
	localeTrack,
});
