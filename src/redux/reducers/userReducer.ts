import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import { getUserSuccessful, getUserError } from '../actions/userActions';
import { setFulfilledState, setRejectedState } from '../utils/cases';

const user = handleActions(
	{
		[String(getUserSuccessful)]: setFulfilledState,
		[String(getUserError)]: setRejectedState,
	},
	{
		data: null,
		error: null,
	}
);

export default combineReducers({
	user,
});
