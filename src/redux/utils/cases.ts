export const setClearState = (state: { data: any, error: any }) => {
	return {
		...state,
		data: null,
		error: null,
	};
};

export const setFulfilledState = (
	state: { data: any, error: any },
	{ payload }: any
) => {
	if (payload?.data) {
		return {
			...state,
			data: payload.data,
			error: null,
		};
	}
	return state;
};

export const setRejectedState = (
	state: { data: any, error: any },
	{ payload: { error } }: any
) => {
	return {
		...state,
		data: null,
		error,
	};
};
