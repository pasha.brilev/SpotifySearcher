export const APP_URL = 'http://localhost:3000/';

export const API_TOKEN_URL = 'https://accounts.spotify.com/api/token';
export const API_BASE_URL = 'https://api.spotify.com';
export const SCOPES =
	'user-read-private user-read-email user-top-read user-read-currently-playing user-read-playback-state user-modify-playback-state';

const AUTHORIZE_URL = new URL('https://accounts.spotify.com/authorize');

AUTHORIZE_URL.searchParams.set('response_type', 'code');
AUTHORIZE_URL.searchParams.set(
	'client_id',
	process.env.REACT_APP_CLIENT_ID || ''
);
AUTHORIZE_URL.searchParams.set('scope', SCOPES);
AUTHORIZE_URL.searchParams.set('redirect_uri', APP_URL);

export { AUTHORIZE_URL };
