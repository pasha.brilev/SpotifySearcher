import axios from 'axios';
import queryString from 'query-string';
import { APP_URL } from 'services/constants';
import api, { headers } from 'services/api';

export const queryParams = queryString.parse(window.location.search);

const body = {
	grant_type: 'authorization_code',
	code: queryParams.code,
	redirect_uri: APP_URL,
	client_id: process.env.REACT_APP_CLIENT_ID,
	client_secret: process.env.REACT_APP_CLIENT_SECRET,
};

export const LogInApi = {
	get: () => {
		return axios.post(
			'https://accounts.spotify.com/api/token',
			queryString.stringify(body),
			{ headers }
		);
	},
};

export const CurrentUserApi = {
	get: () => {
		return api.get(`/v1/me`);
	},
};

export const PlaylistsApi = {
	get: (offset: number) => {
		return api.get(`/v1/me/playlists?offset=${offset}`);
	},
};

export const TraksFromPlaylistApi = {
	get: (playlistId: string, offset: number) => {
		return api.get(
			`/v1/playlists/${playlistId}/tracks?offset=${offset}&limit=50`
		);
	},
};

export const SearchTracksApi = {
	get: (offset: number, search?: string) => {
		let url = search
			? `/v1/search?q=${search}&type=track&offset=${offset}`
			: `/v1/me/top/tracks?offset=${offset}`;
		return api.get(url);
	},
};

export const PlaybackApi = {
	get: () => {
		return api.get(`/v1/me/player`);
	},
};

export const NextTrackApi = {
	post: () => {
		return api.post(`/v1/me/player/next`);
	},
};

export const PreviousTrackApi = {
	post: () => {
		return api.post(`/v1/me/player/previous`);
	},
};

export const PlayApi = {
	put: (body?: { context_uri: string } | { uris: string[] }) => {
		return api.put(`/v1/me/player/play`, body);
	},
};

export const PauseApi = {
	put: () => {
		return api.put(`/v1/me/player/pause`);
	},
};
