import axios from 'axios';
import { getLocaleValue } from 'services/storageApi';
import { API_BASE_URL } from 'services/constants';

export const headers = {
	'Content-Type': 'application/x-www-form-urlencoded',
};

const api = axios.create({
	baseURL: API_BASE_URL,
	headers,
});

api.interceptors.request.use(
	async function (config) {
		const token = getLocaleValue('token');
		config.headers.Authorization = `Bearer ${token}`;

		return config;
	},
	function (error) {
		return Promise.reject(error);
	}
);

export default api;
