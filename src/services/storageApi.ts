export const setLocaleValue = (key: string, value: string) => {
	return localStorage.setItem(key, value);
};

export const getLocaleValue = (key: string) => {
	return localStorage.getItem(key);
};

export const removeLocaleValue = (key: string) => {
	return localStorage.removeItem(key);
};
