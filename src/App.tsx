import React, { useMemo } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Header from 'Components/Header/Header';
import { MainContainer } from './styles';
import Content from 'Components/Content/Content';
import store from 'redux/store';
import LocalePlayer from 'Components/LocalePlayer/LocalePlayer';
import useAuthorization from 'hooks/useAuthorization';

export const AuthorizedContext = React.createContext<{
	isAuthorized: any,
	setIsAuthorized: any,
}>({ isAuthorized: false, setIsAuthorized: () => {} });

function App() {
	let [isAuthorized, setIsAuthorized] = useAuthorization();

	const value = useMemo(
		() => ({
			isAuthorized,
			setIsAuthorized,
		}),
		[isAuthorized, setIsAuthorized]
	);

	return (
		<Provider store={store}>
			<Router>
				<AuthorizedContext.Provider value={value}>
					<MainContainer>
						<Header />
						<Content />
						<LocalePlayer />
					</MainContainer>
				</AuthorizedContext.Provider>
			</Router>
		</Provider>
	);
}

export default App;
